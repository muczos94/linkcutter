namespace webdev.Models
{
    public class Link
    {
        public int Id { get;set; }
        public string Long { get; set; }
        public string Short { get; set; }
    }
}