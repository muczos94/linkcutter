using System.Collections.Generic;
using System.Linq;
using webdev.Interfaces;
using webdev.Models;

namespace webdev.Repository
{
    public class LinksRepository : ILinksRepository
    {
        private List<Link> _links;

        public LinksRepository()
        {
            _links = new List<Link>
            {
                new Link { Id = 0, Long = "www.google.pl/blablabla", Short = "www.google.pl" },
                new Link { Id = 1, Long = "www.google.pl/blablabla", Short = "www.google.pl" }
            };
        }

        public void AddLink(Link link) 
        {
            link.Id = _links.Count;
            _links.Add(link);
        }

        public List<Link> GetLinks() 
        {
            return _links;
        }

        public void Delete(Link link) 
        {
            var linkToDelete = _links
                .SingleOrDefault(element => element.Long == link.Long && element.Short == link.Short);
            _links.Remove(linkToDelete);
        }

        public void DeleteAll()
        {
            _links.Clear();
        }
    }
}