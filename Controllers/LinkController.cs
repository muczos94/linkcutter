using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using webdev.Interfaces;
using webdev.Models;

namespace webdev.Controllers
{
    public class LinkController : Controller
    {
        private ILinksRepository _linksRepository;
        
        public LinkController(ILinksRepository linksRepository)
        {
            _linksRepository = linksRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var links = _linksRepository.GetLinks();
            return View(links);
        }

        [HttpPost]
        public IActionResult Create(Link link)
        {
            _linksRepository.AddLink(link);
            return Redirect("Index");
        }

        [HttpGet]
        public IActionResult Delete(Link link)
        {
            _linksRepository.Delete(link);
            return Redirect("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll()
        {
            _linksRepository.DeleteAll();
            return Redirect("Index");
        }
    }
}